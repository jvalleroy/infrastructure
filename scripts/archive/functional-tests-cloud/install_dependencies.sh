#!/usr/bin/env bash

# This is cloud init script, and thus runs as root

apt-get update
apt-get install -y \
    btrfs-progs \
    git \
    gpg \
    kpartx \
    network-manager \
    openssh-client \
    parted \
    systemd-container \
    wget \
    xz-utils
