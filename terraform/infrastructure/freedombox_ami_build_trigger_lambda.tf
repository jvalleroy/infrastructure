# This block is terraform boilerplate
resource "aws_iam_role" "ami_build_trigger_lambda_role" {
  name = "AMIBuildTrigger"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}


data "archive_file" "ami_build_trigger_lambda_archive" {
  type        = "zip"
  source_file = "${path.module}/freedombox_ami_build_trigger_lambda.py"
  output_path = "${path.module}/files/freedombox_ami_build_trigger_lambda.zip"
}


resource "aws_lambda_function" "ami_build_trigger_lambda_function" {
  function_name    = "freedombox_ami_build_trigger"
  role             = aws_iam_role.ami_build_trigger_lambda_role.arn
  handler          = "freedombox_ami_build_trigger_lambda.handler"
  runtime          = "python3.8"
  filename         = "${path.module}/files/freedombox_ami_build_trigger_lambda.zip"
  source_code_hash = data.archive_file.ami_build_trigger_lambda_archive.output_base64sha256

  tags        = map("Name", "FreedomBox AMI build trigger")
  timeout     = 600
  memory_size = 128
}


resource "aws_iam_policy" "freedombox_ami_build_trigger" {
  description = "Allow triggering CodeBuild pipeline for FreedomBox AMI builds"
  name        = "FreedomBoxAMIBuildTrigger"
  path        = "/"
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "VisualEditor0",
          "Effect" : "Allow",
          "Action" : "codebuild:StartBuild",
          "Resource" : "arn:aws:codebuild:eu-central-1:809319247576:project/cloud-image-builder"
        },
        {
          "Sid" : "VisualEditor1",
          "Effect" : "Allow",
          "Action" : "ec2:DescribeImages",
          "Resource" : "*"
        }
      ]
    }
  )
}


resource "aws_iam_role_policy_attachment" "codebuild-trigger-attachment" {
  role       = aws_iam_role.ami_build_trigger_lambda_role.name
  policy_arn = aws_iam_policy.freedombox_ami_build_trigger.arn
}


resource "aws_iam_role_policy_attachment" "codebuild_trigger_lambda_logging" {
  role       = aws_iam_role.ami_build_trigger_lambda_role.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}


resource "aws_cloudwatch_log_group" "codebuild_trigger_lambda_logs" {
  name              = "/aws/lambda/${aws_lambda_function.ami_build_trigger_lambda_function.function_name}"
  retention_in_days = 14
}
