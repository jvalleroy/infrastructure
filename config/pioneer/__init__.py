# SPDX-License-Identifier: AGPL-3.0-or-later

from buildbot.plugins import schedulers

from .builders import builders

# Run at 1 am on Saturdays
weekly_schedulers = [
    schedulers.Nightly(name='weekly-pioneer', branch='master',
                       builderNames=[builder.name for builder in builders],
                       hour=1, minute=0, dayOfWeek=5)
]

# One force scheduler per builder configuration
force_schedulers = [
    schedulers.ForceScheduler(name="force-{}-build".format(builder.name),
                              builderNames=[builder.name])
    for builder in builders
]

schedulers = weekly_schedulers + force_schedulers
